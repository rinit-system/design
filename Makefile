DOTS=$(shell find . -name *.dot)
PNGS=$(subst .dot,.png,$(DOTS))

all: $(PNGS)

%.png: %.dot
	dot -Tpng $< -o$@
